##Script Gerador de XML para RPS##

###Funcionamento###
Acesse o index passando um valor 'cod', equivalente ao 'codnfse', por GET. O valor será utilizado para acesso ao registro atrelado a este número e a geração de um "xml_cod.xml".

A conexão com o PDO está configurada com base nas linhas 6-9, favor alterar o usuário e senha do mySQL de acordo.

Uma planinha que relaciona as colunas da tabela 'nfse' as respectivas tags [pode ser acessada aqui](https://docs.google.com/spreadsheets/d/1YLc3Yk6zuhPgTyOL3OcePldCtc2biADAQtrdTf8kJ60/edit?usp=sharing).