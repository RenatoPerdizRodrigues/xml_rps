<?php
//Pega o código
$cod = $_GET['cod'];

//Conexão PDO
$mysql_hostname = 'localhost';
$mysql_dbname= 'db_safety';
$mysql_user = 'root';
$mysql_password = '';
$conn = new \PDO("mysql:host={$mysql_hostname};dbname={$mysql_dbname};", $mysql_user, $mysql_password);

//Query que recupera os dados do Tomador
$query = 'SELECT * FROM nfse WHERE codnfse = :cod';
$stmt = $conn->prepare($query);
$stmt->bindValue(':cod', $cod);
$stmt->execute();
$tomador = $stmt->fetchAll(PDO::FETCH_ASSOC);

//Sepada DDD e telefone do Tomador. Ex: (11) 31483-11334, variável $ddd terá valor '11' e a $tel terá valor '31483-11334'
$ddd = substr($tomador[0]['fonefax'], 1, 2);
$tel = substr($tomador[0]['fonefax'], 5, 15);

//Query que recupera os dados do Prestador
$query = 'SELECT * FROM vw_pessoas WHERE codpessoa = 1';
$stmt = $conn->prepare($query);
$stmt->execute();
$prestador = $stmt->fetchAll(PDO::FETCH_ASSOC);

//Sepada DDD e telefone do Prestador.
$dddPrestador = substr($prestador[0]['telcom'], 1, 2);
$telPrestador = substr($prestador[0]['telcom'], 5, 15);

//Array de dados para carregar dinamicamente os valores
$dados = array(
    'LoteRpsID' => $tomador[0]['codnfse'],
    'NumeroLote' => $tomador[0]['lote'],
    'Cnpj' => $tomador[0]['tomacpfcnpj'],
    'InscricaoMunicipal' => '',
    'QuantidadeRps' => '',
    'DataInicio' => '',
    'DataFinal' => '',
    'ValorTotalServicos' => '',
    'ValorTotalDeducoes' => '',
    'InfDeclaracaoPrestacaoServicoID' => '',

    
    //Rps
    'NumeroRps' => '',
    'SerieRps' => '',
    'TipoRps' => '',
    'DataNFSe' => '',
    'DataEmissaoRps' => '',
    'Status' => '',
    
    //Rps substituído
    'NumeroSub' => '',
    'SerieSub' => '',
    'TipoSub' => '',
    'NumeroNFSe' => '',
    'DataNSFeSub' => '',
    'SerieNFSeSubstituido' => '',
    'MotivoSub' => '',
    'CodigoVer' => '',

    //Mais informações de InfDeclaracaoPrestacaoServico
    'DataEmissao' => '',
    'NaturezaOperacao' => '',
    'IncentivadorCultural' => $tomador[0]['incentivadorcultural'],
    'Competencia' => '',

    //Serviço > Valores
    'ValorServicos' => $tomador[0]['valorservicos'],
    'ValorDeducoes' => $tomador[0]['valordeducoes'],
    'ValorPis' => $tomador[0]['valorpis'],
    'ValorCofins' => $tomador[0]['valorcofins'],
    'ValorInss' => $tomador[0]['valorinss'],
    'ValorIr' => $tomador[0]['valorir'],
    'ValorCsll' => $tomador[0]['valorcsll'],
    'ValorIss' => $tomador[0]['valoriss'],
    'ValorIssRetido' => $tomador[0]['valorissretido'],
    'OutrasRetencoes' => $tomador[0]['outrasretencoes'],
    'BaseCalculo' => $tomador[0]['basecalcicms'],
    'Aliquota' => '',
    'ValorLiquidoNfse' => '',
    'DescontoIncondicionado' => $tomador[0]['descontoincondicionado'],
    'DescontoCondicionado' => $tomador[0]['descontocondicionado'],
    'AliquotaPIS' => '',
    'AliquotaCOFINS' => '',
    'AliquotaIR' => '',
    'AliquotaCSLL' => '',
    'AliquotaINSS' => '',

    //Serviço
    'IssRetido' => $tomador[0]['issretido'],
    'ResponsavelRetencao' => '',
    'ItemListaServico' => '',
    'CodigoCnae' => '',
    'CodigoTributacaoMunicipio' => '',
    'Discriminacao' => $tomador[0]['discriminacao'],
    'CodigoMunicipio' => '',
    'ExigibilidadeISS' => '',
    'MunicipioIncidencia' => '',
    'IndicaST' => '',
    'CEPPrestacao' => '',
    'CidadePrestacao' => '',
    'EstadoPrestacao' => '',
    'EnderecoPrestacao' => '',
    'NumeroPrestacao' => '',
    'ComplementoPrestacao' => '',
    'BairroPrestacao' => '',
    'LocalPrestacaoServico' => '',
    'ServicoPrestadoViasPublicas' => '',
    'NumeroEmissorEquiplano' => '',
    'VersaoXMLEquiplano' => '',
    'ServicoPrestadoEhExportacao' => '',

    //Prestador
    'CnpjPrestador' => $prestador[0]['cnpj'],
    'InscricaoMunicipalPrestador' => '',
    'RazaoSocialPrestador' => $prestador[0]['razao'],
    'DDDPrestador' => $dddPrestador,
    'TelefonePrestador' => $telPrestador,
    'ChaveAutenticacao' => '',
    'ChaveAcesso' => '',
    'CPFResponsavel' => $prestador[0]['cpf'],
    'EnderecoPrestador' => '',
    'NumeroPrestador' => '',
    'BairroPrestador' => '',
    'CepPrestador' => '',

    //Tomador
    //Precisa de ID Tomador, por exemplo?
    'CnpjTomador' => $tomador[0]['tomacpfcnpj'],
    'InscricaoMunicipalTomador' => '',
    'DocumentoEstrangeiro' => '',
    'InscricaoEstadual' => $tomador[0]['inscestadual'],
    'RazaoSocialTomador' => $tomador[0]['nomerazao'],
    'Siaf' => '',
    'TipoLogr' => $tomador[0]['tomaendlogradouro'],
    'EnderecoTomador' => $tomador[0]['tomaendlogradouro'],
    'NumeroTomador' => $tomador[0]['tomaendnumero'],
    'ComplementoTomador' => $tomador[0]['tomaendcomplemento'],
    'BairroTomador' => $tomador[0]['tomaendbairro'],
    'CodigoMunicipioTomador' => '',
    'xPais' => '',
    'Municipio' => $tomador[0]['tomaendxmun'],
    'Uf' => $tomador[0]['tomaenduf'],
    'Cep' => $tomador[0]['tomaendcep'],
    'DDDTomador' => $ddd,
    'Telefone' => $tel,
    'Email' => $tomador[0]['tomaemail'],

    //Intermediário
    'CnpjIntermediario' => '',
    'InscricaoMunicipalIntermediario' => '',
    'EmailIntermediario' => '',
    'RazaoSocialIntermediario' => '',
    'EnderecoIntermediario' => '',
    'BairroIntermediario' => '',
    'NumeroIntermediario' => '',
    'CepIntermediario' => '',
    'CodigoMunicipioIntermediario' => '',

    //Construção Civil
    'CodigoObra' => '',
    'Art' => '',
    'RegimeEspecialTributacao' => '',
    'OptanteSimplesNacional' => '',
    'IncentivoFiscal' => '',
    'SeriePrestacao' => '',
    'TipoRecolhimento' => '',
    'Tributacao' => '',
    'Operacao' => '',
    'NumeroLoteConstrucao' => '',
    'CodigoVerificacao' => '',
    'UsuarioIntegra' => '',
    'SenhaIntegra' => '',
    'JustificativaCancelamento' => '',

    //Dedução
    'DeducaoPor' => '',
    'TipoDeducao' => '',
    'CNPJCPFRef' => '',
    'NumeroNFRef' => '',
    'ValorTotalRef' => '',
    'PercentualDed' => '',
    'ValorDeduzir' => '',
    'IndSincronoAssincrono' => '',
    'SerieNFSe' => '',
    'tpAmb' => '',

    //DuplFatura
    'NumeroDaFatura' => '',
    'ValorDaFatura' => '',
    'DataVencimento' => '',
    'FormaDePagamentoDaFatura' => '',
);

//Geração de XML
//Inicialização do DOM e formatação do XML
$dom = new DOMDocument('1.0', 'utf-8');
$dom->formatOutput = true;

//Abertura das duas tags que envolvem todas as outras, EnviarLoteRpsEnvio e LoteRps
$enviarLote = $dom->createElement('EnviarLoteRpsEnvio');
$loteRps = $dom->createElement('LoteRps');
$loteRps->setAttribute('Id', $dados['LoteRpsID']);

$dom->appendChild($enviarLote);
$enviarLote->appendChild($loteRps);

    //Tags dentro de LoteRps
    $numeroLote = $dom->createElement('NumeroLote', $dados['NumeroLote']);
    $cpfCnpj = $dom->createElement('CpfCnpj');
    $cnpj = $dom->createElement('Cnpj', $dados['Cnpj']);
    $inscricaoMunicipal = $dom->createElement('InscricaoMunicipal', $dados['InscricaoMunicipal']);
    $qtdRps = $dom->createElement('QuantidadeRps', $dados['QuantidadeRps']);
    $dataInicio = $dom->createElement('DataInicio', $dados['DataInicio']);
    $dataFinal = $dom->createElement('DataFinal', $dados['DataFinal']);
    $valorTotalServicos = $dom->createElement('ValorTotalServicos', $dados['ValorTotalServicos']);
    $valorTotalDeducoes = $dom->createElement('ValorTotalDeducoes', $dados['ValorTotalDeducoes']);

    $loteRps->appendChild($numeroLote);
    $loteRps->appendChild($cpfCnpj);
    $cpfCnpj->appendChild($cnpj);
    $loteRps->appendChild($inscricaoMunicipal);
    $loteRps->appendChild($qtdRps);
    $loteRps->appendChild($dataInicio);
    $loteRps->appendChild($dataFinal);
    $loteRps->appendChild($valorTotalServicos);
    $loteRps->appendChild($valorTotalDeducoes);
    
    //Tags dentro de ListaRps
    $listaRps = $dom->createElement('ListaRps');
    $rps = $dom->createElement('Rps');
    $rps1 = $dom->createElement('Rps');
    $infDeclaracao = $dom->createElement('InfDeclaracaoPrestacaoServico');
    $infDeclaracao->setAttribute('Id', $dados['InfDeclaracaoPrestacaoServicoID']);

    $loteRps->appendChild($listaRps);
    $listaRps->appendChild($rps);
    $rps->appendChild($infDeclaracao);
    $infDeclaracao->appendChild($rps1);

    //Tags dentro de Rps
    $rps2 = $dom->createElement('Rps');
    $identificacaoRps = $dom->createElement('IdentificacaoRps');
    $numero = $dom->createElement('Numero', $dados['NumeroRps']);
    $serie = $dom->createElement('Serie', $dados['SerieRps']);
    $tipo = $dom->createElement('Tipo', $dados['TipoRps']);
    $dataNfs = $dom->createElement('DataNFSeSubstituicao', $dados['DataNFSe']);
    $dataEmissao = $dom->createElement('DataEmissao', $dados['DataEmissaoRps']);
    $status = $dom->createElement('Status', $dados['Status']);
    $rpsSubstituido = $dom->createElement('RpsSubstituido');
    $numeroSubstituido = $dom->createElement('Numero', $dados['NumeroSub']);
    $serieSubstituido = $dom->createElement('Serie', $dados['SerieSub']);
    $tipoSubstituido = $dom->createElement('Tipo', $dados['TipoSub']);
    $numeroNfseSubstituido = $dom->createElement('NumeroNFSe', $dados['NumeroNFSe']);
    $dataNfsSubstituido = $dom->createElement('DataNFSeSubstituicao', $dados['DataNSFeSub']);
    $serieNfsSubstituido = $dom->createElement('SerieNFSeSubstituido', $dados['SerieNFSeSubstituido']);
    $motivoSubstituido = $dom->createElement('MotivoSubstituicao', $dados['MotivoSub']);
    $codigoVerificaoSub = $dom->createElement('CodigoVerificacaoSubstituicao', $dados['CodigoVer']);

    $rps1->appendChild($identificacaoRps);
    $identificacaoRps->appendChild($numero);
    $identificacaoRps->appendChild($serie);
    $identificacaoRps->appendChild($tipo);
    $identificacaoRps->appendChild($dataNfs);
    $rps1->appendChild($dataEmissao);
    $rps1->appendChild($status);
    $rps1->appendChild($rpsSubstituido);
    $rpsSubstituido->appendChild($numeroSubstituido);
    $rpsSubstituido->appendChild($serieSubstituido);
    $rpsSubstituido->appendChild($tipoSubstituido);
    $rpsSubstituido->appendChild($numeroNfseSubstituido);
    $rpsSubstituido->appendChild($dataNfsSubstituido);
    $rpsSubstituido->appendChild($serieNfsSubstituido);
    $rpsSubstituido->appendChild($motivoSubstituido);
    $rpsSubstituido->appendChild($codigoVerificaoSub);

    //Tags entre a rps e servico
    $dataEmissao2 = $dom->createElement('DataEmissao', $dados['DataEmissao']);
    $naturezaOp = $dom->createElement('NaturezaOperacao', $dados['NaturezaOperacao']);
    $incentivadorCultural = $dom->createElement('IncentivadorCultural', $dados['IncentivadorCultural']);
    $competencia = $dom->createElement('Competencia', $dados['Competencia']);
    $servico = $dom->createElement('Servico');

    $infDeclaracao->appendChild($dataEmissao2);
    $infDeclaracao->appendChild($naturezaOp);
    $infDeclaracao->appendChild($incentivadorCultural);
    $infDeclaracao->appendChild($competencia);
    $infDeclaracao->appendChild($servico);

    //Tags dentro de servico > valores
    $valores = $dom->createElement('Valores');
    $valorServicos = $dom->createElement('ValorServicos', $dados['ValorServicos']);
    $valorDeducoes = $dom->createElement('ValorDeducoes', $dados['ValorDeducoes']);
    $valorPis = $dom->createElement('ValorPis', $dados['ValorPis']);
    $valorCofins = $dom->createElement('ValorCofins', $dados['ValorCofins']);
    $valorInss = $dom->createElement('ValorInss', $dados['ValorInss']);
    $valorIr = $dom->createElement('ValorIr', $dados['ValorIr']);
    $valorCsll = $dom->createElement('ValorCsll', $dados['ValorCsll']);
    $valorIss = $dom->createElement('ValorIss', $dados['ValorIss']);
    $valorIssRetido = $dom->createElement('ValorIssRetido', $dados['ValorIssRetido']);
    $outrasRetencoes = $dom->createElement('OutrasRetencoes', $dados['OutrasRetencoes']);
    $baseCalculo = $dom->createElement('BaseCalculo', $dados['BaseCalculo']);
    $aliquota = $dom->createElement('Aliquota', $dados['Aliquota']);
    $valorLiquidoNfse = $dom->createElement('ValorLiquidoNfse', $dados['ValorLiquidoNfse']);
    $descontoIncondicionado = $dom->createElement('DescontoIncondicionado', $dados['DescontoIncondicionado']);
    $descontoCondicionado = $dom->createElement('DescontoCondicionado', $dados['DescontoCondicionado']);
    $aliquotaPis = $dom->createElement('AliquotaPIS', $dados['AliquotaPIS']);
    $aliquotaCofins = $dom->createElement('AliquotaCOFINS', $dados['AliquotaCOFINS']);
    $aliquotaIr = $dom->createElement('AliquotaIR', $dados['AliquotaIR']);
    $aliquotaCsll = $dom->createElement('AliquotaCSLL', $dados['AliquotaCSLL']);
    $aliquotaInss = $dom->createElement('AliquotaINSS', $dados['AliquotaINSS']);

    $servico->appendChild($valores);
    $valores->appendChild($valorServicos);
    $valores->appendChild($valorDeducoes);
    $valores->appendChild($valorPis);
    $valores->appendChild($valorCofins);
    $valores->appendChild($valorInss);
    $valores->appendChild($valorIr);
    $valores->appendChild($valorCsll);
    $valores->appendChild($valorIss);
    $valores->appendChild($valorIssRetido);
    $valores->appendChild($outrasRetencoes);
    $valores->appendChild($baseCalculo);
    $valores->appendChild($aliquota);
    $valores->appendChild($valorLiquidoNfse);
    $valores->appendChild($descontoIncondicionado);
    $valores->appendChild($descontoCondicionado);
    $valores->appendChild($aliquotaPis);
    $valores->appendChild($aliquotaCofins);
    $valores->appendChild($aliquotaIr);
    $valores->appendChild($aliquotaCsll);
    $valores->appendChild($aliquotaInss);

    //Tags dentro de servico
    $issRetido = $dom->createElement('IssRetido', $dados['IssRetido']);
    $responsavelRetencao = $dom->createElement('ResponsavelRetencao', $dados['ResponsavelRetencao']);
    $itemListaServico = $dom->createElement('ItemListaServico', $dados['ItemListaServico']);
    $codigoCnae = $dom->createElement('CodigoCnae', $dados['CodigoCnae']);
    $codigoTributacaoMunicipio = $dom->createElement('CodigoTributacaoMunicipio', $dados['CodigoTributacaoMunicipio']);
    $discriminacao = $dom->createElement('Discriminacao', $dados['Discriminacao']);
    $codigoMunicipio = $dom->createElement('CodigoMunicipio', $dados['CodigoMunicipio']);
    $exigibilidadeIss = $dom->createElement('ExigibilidadeISS', $dados['ExigibilidadeISS']);
    $municipioIncidencia = $dom->createElement('MunicipioIncidencia', $dados['MunicipioIncidencia']);
    $indicaSt = $dom->createElement('IndicaST', $dados['IndicaST']);
    $cepPrestacao = $dom->createElement('CEPPrestacao', $dados['CEPPrestacao']);
    $cidadePrestacao = $dom->createElement('CidadePrestacao', $dados['CidadePrestacao']);
    $estadoPrestacao = $dom->createElement('EstadoPrestacao', $dados['EstadoPrestacao']);
    $enderecoPrestacao = $dom->createElement('EnderecoPrestacao', $dados['EnderecoPrestacao']);
    $numeroPrestacao = $dom->createElement('NumeroPrestacao', $dados['NumeroPrestacao']);
    $complementoPrestacao = $dom->createElement('ComplementoPrestacao', $dados['ComplementoPrestacao']);
    $bairroPrestacao = $dom->createElement('BairroPrestacao', $dados['BairroPrestacao']);
    $localPrestacao = $dom->createElement('LocalPrestacaoServico', $dados['LocalPrestacaoServico']);
    $servicoPrestado = $dom->createElement('ServicoPrestadoViasPublicas', $dados['ServicoPrestadoViasPublicas']);
    $numeroEmissor = $dom->createElement('NumeroEmissorEquiplano', $dados['NumeroEmissorEquiplano']);
    $versaoXml = $dom->createElement('VersaoXMLEquiplano', $dados['VersaoXMLEquiplano']);
    $servicoPrestadoExportacao = $dom->createElement('ServicoPrestadoEhExportacao', $dados['ServicoPrestadoEhExportacao']);
    
    $servico->appendChild($issRetido);
    $servico->appendChild($responsavelRetencao);
    $servico->appendChild($itemListaServico);
    $servico->appendChild($codigoCnae);
    $servico->appendChild($codigoTributacaoMunicipio);
    $servico->appendChild($discriminacao);
    $servico->appendChild($codigoMunicipio);
    $servico->appendChild($exigibilidadeIss);
    $servico->appendChild($municipioIncidencia);
    $servico->appendChild($indicaSt);
    $servico->appendChild($cepPrestacao);
    $servico->appendChild($cidadePrestacao);
    $servico->appendChild($enderecoPrestacao);
    $servico->appendChild($estadoPrestacao);
    $servico->appendChild($numeroPrestacao);
    $servico->appendChild($complementoPrestacao);
    $servico->appendChild($bairroPrestacao);
    $servico->appendChild($localPrestacao);
    $servico->appendChild($localPrestacao);
    $servico->appendChild($servicoPrestado);
    $servico->appendChild($numeroEmissor);
    $servico->appendChild($versaoXml);
    $servico->appendChild($servicoPrestadoExportacao);

    //Tags dentro de prestador
    $prestador = $dom->createElement('Prestador');
    $cpfCnpjPrestador = $dom->createElement('CpfCnpj');
    $cnpjPrestador = $dom->createElement('Cnpj', $dados['CnpjPrestador']);
    $inscricaoMunicipalPrestador = $dom->createElement('InscricaoMunicipal', $dados['InscricaoMunicipalPrestador']);
    $razaoSocialPrestador = $dom->createElement('RazaoSocialPrestador', $dados['RazaoSocialPrestador']);
    $DddPrestador = $dom->createElement('DDDPrestador', $dados['DDDPrestador']);
    $telPrestador = $dom->createElement('TelefonePrestador', $dados['TelefonePrestador']);
    $chaveAutenticacao = $dom->createElement('ChaveAutenticacao', $dados['ChaveAutenticacao']);
    $chaveAcesso = $dom->createElement('ChaveAcesso', $dados['ChaveAcesso']);
    $CpfResponsavel = $dom->createElement('CPFResponsavel', $dados['CPFResponsavel']);
    $endereco = $dom->createElement('Endereco', $dados['EnderecoPrestador']);
    $numero = $dom->createElement('Numero', $dados['NumeroPrestador']);
    $bairro = $dom->createElement('Bairro', $dados['BairroPrestador']);
    $cep = $dom->createElement('Cep', $dados['CepPrestador']);

    $infDeclaracao->appendChild($prestador);
    $prestador->appendChild($cpfCnpjPrestador);
    $cpfCnpjPrestador->appendChild($cnpjPrestador);
    $prestador->appendChild($inscricaoMunicipalPrestador);
    $prestador->appendChild($razaoSocialPrestador);
    $prestador->appendChild($DddPrestador);
    $prestador->appendChild($telPrestador);
    $prestador->appendChild($chaveAutenticacao);
    $prestador->appendChild($chaveAcesso);
    $prestador->appendChild($CpfResponsavel);
    $prestador->appendChild($endereco);
    $prestador->appendChild($numero);
    $prestador->appendChild($bairro);
    $prestador->appendChild($cep);

    //Tags dentro de tomador
    $tomador = $dom->createElement('Tomador');
    $idTomador = $dom->createElement('IdentificacaoTomador');
    $cpfCnpjTomador = $dom->createElement('CpfCnpj');
    $cnpjTomador = $dom->createElement('Cnpj', $dados['CnpjTomador']);
    $inscricaoMunicipalTomador = $dom->createElement('InscricaoMunicipal', $dados['InscricaoMunicipalTomador']);
    $documentoEstrangeiro = $dom->createElement('DocumentoEstrangeiro', $dados['DocumentoEstrangeiro']);
    $inscricaoEstadual = $dom->createElement('InscricaoEstadual', $dados['InscricaoEstadual']);
    $razaoSocialTomador = $dom->createElement('RazaoSocial', $dados['RazaoSocialTomador']);
    $siaf = $dom->createElement('Siaf', $dados['Siaf']);
    $enderecoTomador = $dom->createElement('Endereco');
    $tipoLogr = $dom->createElement('TipoLogr', $dados['TipoLogr']);
    $enderecoTomador2 = $dom->createElement('Endereco', $dados['EnderecoTomador']);
    $numeroTomador = $dom->createElement('Numero', $dados['NumeroTomador']);
    $complementoTomador = $dom->createElement('Complemento', $dados['ComplementoTomador']);
    $bairroTomador = $dom->createElement('Bairro', $dados['BairroTomador']);
    $codigoMunicipioTomador = $dom->createElement('CodigoMunicipio', $dados['CodigoMunicipioTomador']);
    $xPais = $dom->createElement('xPais', $dados['xPais']);
    $municipioTomador = $dom->createElement('Municipio', $dados['Municipio']);
    $ufTomador = $dom->createElement('Uf', $dados['Uf']);
    $cepTomador = $dom->createElement('Cep', $dados['Cep']);
    $contatoTomador = $dom->createElement('Contato');
    $dddTomador = $dom->createElement('DDDTomador', $ddd);
    $telefoneTomador = $dom->createElement('Telefone', $tel);
    $emailTomador = $dom->createElement('Email', $dados['Email']);

    $infDeclaracao->appendChild($tomador);
    $tomador->appendChild($idTomador);
    $idTomador->appendChild($cpfCnpjTomador);
    $cpfCnpjTomador->appendChild($cnpjTomador);
    $idTomador->appendChild($inscricaoMunicipalTomador);
    $idTomador->appendChild($documentoEstrangeiro);
    $idTomador->appendChild($inscricaoEstadual);
    $tomador->appendChild($razaoSocialTomador);
    $tomador->appendChild($siaf);
    $tomador->appendChild($enderecoTomador);
    $enderecoTomador->appendChild($tipoLogr);
    $enderecoTomador->appendChild($enderecoTomador2);
    $enderecoTomador->appendChild($numeroTomador);
    $enderecoTomador->appendChild($complementoTomador);
    $enderecoTomador->appendChild($bairroTomador);
    $enderecoTomador->appendChild($codigoMunicipioTomador);
    $enderecoTomador->appendChild($xPais);
    $enderecoTomador->appendChild($municipioTomador);
    $enderecoTomador->appendChild($ufTomador);
    $enderecoTomador->appendChild($cepTomador);
    $tomador->appendChild($contatoTomador);
    $contatoTomador->appendChild($dddTomador);
    $contatoTomador->appendChild($telefoneTomador);
    $contatoTomador->appendChild($emailTomador);

    //Tags de intermediario
    $intermediario = $dom->createElement('Intermediario');
    $idIntermediario = $dom->createElement('IdentificacaoIntermediario');
    $cpfCnpjIntermediario = $dom->createElement('CpfCnpj');
    $cnpjIntermediario = $dom->createElement('Cnpj', $dados['CnpjIntermediario']);
    $inscricaoMunicipalIntermediario = $dom->createElement('InscricaoMunicipal', $dados['InscricaoMunicipalIntermediario']);
    $emailIntermediario = $dom->createElement('EmailIntermediario', $dados['EmailIntermediario']);
    $razaoSocialIntermediario = $dom->createElement('RazaoSocial', $dados['RazaoSocialIntermediario']);
    $enderecoIntermediario = $dom->createElement('Endereco', $dados['EnderecoIntermediario']);
    $bairroIntermediario = $dom->createElement('Bairro', $dados['BairroIntermediario']);
    $numeroIntermediario = $dom->createElement('Numero', $dados['NumeroIntermediario']);
    $cepIntermediario = $dom->createElement('Cep', $dados['CepIntermediario']);
    $codigoMunicipioIntermediario = $dom->createElement('CodigoMunicipio', $dados['CodigoMunicipioIntermediario']);

    $infDeclaracao->appendChild($intermediario);
    $intermediario->appendChild($idIntermediario);
    $idIntermediario->appendChild($cpfCnpjIntermediario);
    $cpfCnpjIntermediario->appendChild($cnpjIntermediario);
    $idIntermediario->appendChild($inscricaoMunicipalIntermediario);
    $idIntermediario->appendChild($emailIntermediario);
    $intermediario->appendChild($razaoSocialIntermediario);
    $intermediario->appendChild($enderecoIntermediario);
    $intermediario->appendChild($bairroIntermediario);
    $intermediario->appendChild($numeroIntermediario);
    $intermediario->appendChild($cepIntermediario);
    $intermediario->appendChild($cepIntermediario);
    $intermediario->appendChild($codigoMunicipioIntermediario);

    //Tags de construcaocivil e outras
    $construcaoCivil = $dom->createElement('ConstrucaoCivil');
    $codigoObra = $dom->createElement('CodigoObra', $dados['CodigoObra']);
    $art = $dom->createElement('Art', $dados['Art']);
    $regimeEspecial = $dom->createElement('RegimeEspecialTributacao', $dados['RegimeEspecialTributacao']);
    $optanteSimples = $dom->createElement('OptanteSimplesNacional', $dados['OptanteSimplesNacional']);
    $incentivoFiscal = $dom->createElement('IncentivoFiscal', $dados['IncentivoFiscal']);
    $seriePrestacao = $dom->createElement('SeriePrestacao', $dados['SeriePrestacao']);
    $tipoRecolhimento = $dom->createElement('TipoRecolhimento', $dados['TipoRecolhimento']);
    $tributacao = $dom->createElement('Tributacao', $dados['Tributacao']);
    $operacao = $dom->createElement('Operacao', $dados['Operacao']);
    $numeroLoteConstrucao = $dom->createElement('NumeroLote', $dados['NumeroLoteConstrucao']);
    $codigoVerificacao = $dom->createElement('CodigoVerificacao', $dados['CodigoVerificacao']);
    $usuarioIntegra = $dom->createElement('UsuarioIntegra', $dados['UsuarioIntegra']);
    $senhaIntegra = $dom->createElement('SenhaIntegra', $dados['SenhaIntegra']);
    $justificativaCancelamento = $dom->createElement('JustificativaCancelamento', $dados['JustificativaCancelamento']);

    $infDeclaracao->appendChild($construcaoCivil);
    $construcaoCivil->appendChild($codigoObra);
    $construcaoCivil->appendChild($art);
    $infDeclaracao->appendChild($regimeEspecial);
    $infDeclaracao->appendChild($optanteSimples);
    $infDeclaracao->appendChild($incentivoFiscal);
    $infDeclaracao->appendChild($seriePrestacao);
    $infDeclaracao->appendChild($tipoRecolhimento);
    $infDeclaracao->appendChild($tributacao);
    $infDeclaracao->appendChild($operacao);
    $infDeclaracao->appendChild($numeroLoteConstrucao);
    $infDeclaracao->appendChild($codigoVerificacao);
    $infDeclaracao->appendChild($usuarioIntegra);
    $infDeclaracao->appendChild($senhaIntegra);
    $infDeclaracao->appendChild($justificativaCancelamento);

    //Tags dentro de deducao
    $deducao = $dom->createElement('Deducao');
    $tcDeducao = $dom->createElement('tcDeducao');
    $deducaoPor = $dom->createElement('DeducaoPor', $dados['DeducaoPor']);
    $tipoDeducao = $dom->createElement('TipoDeducao', $dados['TipoDeducao']);
    $cnpjcpfRef = $dom->createElement('CNPJCPFRef', $dados['CNPJCPFRef']);
    $numeroNfref = $dom->createElement('NumeroNFRef', $dados['NumeroNFRef']);
    $valorTotalRef = $dom->createElement('ValorTotalRef', $dados['ValorTotalRef']);
    $percentualDed = $dom->createElement('PercentualDed', $dados['PercentualDed']);
    $valorDeduzir = $dom->createElement('ValorDeduzir', $dados['ValorDeduzir']);
    $indSincrono = $dom->createElement('IndSincronoAssincrono', $dados['IndSincronoAssincrono']);
    $serieNfse = $dom->createElement('SerieNFSe', $dados['SerieNFSe']);
    $tpAmb = $dom->createElement('tpAmb', $dados['tpAmb']);

    $infDeclaracao->appendChild($deducao);
    $deducao->appendChild($tcDeducao);
    $tcDeducao->appendChild($deducaoPor);
    $tcDeducao->appendChild($tipoDeducao);
    $tcDeducao->appendChild($cnpjcpfRef);
    $tcDeducao->appendChild($numeroNfref);
    $tcDeducao->appendChild($valorTotalRef);
    $tcDeducao->appendChild($percentualDed);
    $tcDeducao->appendChild($valorDeduzir);
    $infDeclaracao->appendChild($indSincrono);
    $infDeclaracao->appendChild($serieNfse);
    $infDeclaracao->appendChild($tpAmb);

    //Tags dentro de DuplFatura
    $duplFatura = $dom->createElement('DuplFatura');
    $tcFatura = $dom->createElement('tcFaturaServ');
    $numeroFatura = $dom->createElement('NumeroDaFatura', $dados['NumeroDaFatura']);
    $valorDaFatura = $dom->createElement('ValorDaFatura', $dados['ValorDaFatura']);
    $dataVencimento = $dom->createElement('DataVencimento', $dados['DataVencimento']);
    $formaDePagamento = $dom->createElement('FormaDePagamentoDaFatura', $dados['FormaDePagamentoDaFatura']);

    $infDeclaracao->appendChild($duplFatura);
    $duplFatura->appendChild($tcFatura);
    $tcFatura->appendChild($numeroFatura);
    $tcFatura->appendChild($valorDaFatura);
    $tcFatura->appendChild($dataVencimento);
    $tcFatura->appendChild($formaDePagamento);

    $dom->save('rps_'. $cod .'.xml');
?>